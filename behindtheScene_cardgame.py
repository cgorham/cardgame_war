"""module containing Card, Deck, Hand, and
Player classes for a cardgame
By: Caroline Gorham, 19 March 2013"""

class Cards(object):
    """holds information for a deck of French cards:
    __init__(suit,val) -- card_id (Cards.i=1:52), Value from Cards.strVALUE, Suit from Cards.strSUIT,
    rank: [2..14]=[2..Ace], facing:[up,down], and a len attribute to make for easier processing.
    __str__()-- prints the Cards information with the standard description VALUE of SUIT -- RANK
    """
    
    strSUIT=['Clubs','Diamonds','Hearts','Spades']
    strVALUE=['Joker', '2','3','4', '5' ,'6','7', '8' ,'9','10', 'J' ,'Q','K', 'Ace']
    
    i=1
    
    def __init__ (self, SUIT, VALUE):
        self.card_id=Cards.i    
        self.rank=(self.card_id+1)-(13*SUIT)         
        self.value=Cards.strVALUE[VALUE]
        self.suit=Cards.strSUIT[SUIT]
        self.facing=bool(False)             # True-face up, False-face down
        self.len='1'
        
        Cards.i=Cards.i+1

    def __str__ (self):
        print_it=self.value +" of " + self.suit
        return  print_it + "  ------- Rank: %s" %self.rank
        


class Deck(Cards):
    """Deck is a set of cards,
    __init__ ()-- builds the set of Cards ; print_deck -- prints Cards in a fasionable way ;
    deal(num) shuffles the deck and returns 'num' cards """
    
    def __init__ (self):
        self.Cards=[]
        for suit in range(len(Cards.strSUIT)):
            for value in range(1, len(Cards.strVALUE)):
                self.Cards.append(Cards(suit, value))
  
    def print_deck (self):
        print "***Original Deck****************************"
        for card in self.Cards:
           print "%s" %card 
        print "********************************************"

    def deal (self, num_in_deal):
        import random
        import numpy as np
        
        random.shuffle(self.Cards)

        card=self.Cards[0]
        del(self.Cards[0])
        self.remaining=self.remaining-1
        return card
    
class Hand(Deck):
    """ Hand is a set of Cards from the Deck,
    __init__() initializes the Cards ; get_cards(deck, num) builds the hand with  'num'
    Cards that the deck deals ; win_cards(*args) appends any amount of Cards to the hand ;
    print_hand() prints the hand in a fashionable way"""

    def __init__(self):
        self.Cards=[]
        self.name="Computer"

    def get_cards(self, deck, num_in_deal):
        for index in xrange(num_in_deal):
            self.Cards.append(deck.deal(num_in_deal))

    def win_cards(self, *args):

        for x in args:
            try:
                x.len
                self.Cards.append(x)
            except:
                for cell in xrange(len(x)):
                    self.Cards.append(x[cell])
                

    def print_hand (self):
        print "*** %s" %self.name +"'s Hand***************************"
        for card in self.Cards:
            
            print "%s" %card
        print "********************************************"


class Player(Hand):
    """The player has a Hand, that is a set of Cards from the Deck -
    __init__() initilizes the hand, the name of the player holding the hand, and the score ;
    print_card(card) prints the card that the player is holding, most commonly the one being
    played"""

    def __init__ (self):

        self.hand=Hand()
        self.hand.name=""
        self.score=0

    def print_card (self,card):
        print self.hand.name +"'s Hand "
        print card
    
