#!/usr/bin/env python
""" This code plays a game of war, a game requiring
no skill or user input. The module behindtheScene_cardgame
provides the classes Cards, Deck, Hand, and Player
which interact in this script to build a deck from cards,
hands for the players from the deck, and the player uses
their hand to play the game. Changes to the players hand are
made through the battle_sequence and war_sequence - dependent on
neccessity.
By: Caroline Gorham, 19 March 2013
"""


import sys
from optparse import OptionParser
import behindtheScene_cardgame

## variables ##

call_deal_ntimes_in_turn=1   # times dealer deals to player in turn 

############################# 
    
def battle_sequence(winner, loser, round_of_play, card_index):
    """This function performs the necessary actions to the player's hands
    during a battle, returning the player instances to the main function"""
        
    # turn played cards back over
    winner.hand.Cards[card_index].facing=bool(False)
    loser.hand.Cards[card_index].facing=bool(False)


    # winner puts cards at bottom of deck
    winner.hand.win_cards(winner.hand.Cards[card_index], loser.hand.Cards[card_index])

    # remove played cards from top of deck
    del winner.hand.Cards[card_index]
    del loser.hand.Cards[card_index] 

    # establish new scores
    winner.score= len(winner.hand.Cards)
    loser.score= len(loser.hand.Cards)
        
    print winner.hand.name + " wins round %d" %round_of_play
    print "::~~~~~~::"
    print "::~~~~~~::"

    return winner, loser

def war_sequence(winner, loser, round_of_play, card_index):
    """This function performs the necessary actions to the player's hands
    during a war, returning the player instances to the main function"""

    # turn played cards back over
    for each in xrange(len(winner.hand.Cards)):
        winner.hand.Cards[each].facing=bool(False)
    for each in xrange(len(loser.hand.Cards)):
        loser.hand.Cards[each].facing=bool(False)

    # winner puts cards at bottom of deck       
    winner.hand.win_cards(winner.hand.Cards[:(card_index+1)],loser.hand.Cards[:(card_index+1)])

    # remove played cards from top of deck
    del winner.hand.Cards[:(card_index+1)]
    del loser.hand.Cards[:(card_index+1)]

    # establish new scores
    winner.score= len(winner.hand.Cards)
    loser.score= len(loser.hand.Cards)
    
    print winner.hand.name + " wins round %d" %round_of_play
    print "::~~~~~~::"
    print "::~~~~~~::"
    
    return winner, loser

def printscores (p1, p2):
    print p1.hand.name + " has %d" %p1.score +" cards"
    print p2.hand.name + " has %d" %p2.score +" cards"
    return None

def printwarcards (p1, p2, p1warcard, p2warcard ):
    p1.print_card(p1warcard)
    p2.print_card(p2warcard)
    return None

def flip_assign(p1, p2, card_index):
    """flips card, assigns as wildcard, prints the card and optionally
    prints the statement of 'player has X cards' """

    #flip wildcard
    p1.hand.Cards[card_index].facing=bool(True)
    p2.hand.Cards[card_index].facing=bool(True)

    p1warcard=p1.hand.Cards[card_index]
    p2warcard=p2.hand.Cards[card_index]

    return p1, p2, p1warcard, p2warcard


def main():
    """The main function -- parses the command line, sets up required players,
    makes the deck, deals the deck into player hands, then plays WAR with the
    players hands until a winner is established."""

    # command line parser 
    usage = "usage: %prog [options]  --p1=opt1 --p2=opt2 "

    parser = OptionParser(usage=usage, version="%prog 1.0")

    parser.add_option("--p1", type="string", dest="option1", default="Computer1",
                     help="Enter the name of player 1 - Default=Comp")
 

    parser.add_option("--p2", type="string", dest="option2", default="Computer2",
                  help="Enter the name of player 2 - Default=Comp")

    (options, args) = parser.parse_args()

    # set up players with names accordingly
    p1=behindtheScene_cardgame.Player()
    p1.hand.name=(options.option1)
    
    p2=behindtheScene_cardgame.Player()
    p2.hand.name=(options.option2)
    
    # make the deck and count the deck
    my_deck=behindtheScene_cardgame.Deck()
    my_deck.print_deck()
    my_deck.remaining=len(my_deck.Cards)

    # deal the deck to the players in sets of
    # a certain number of cards until that method would
    # be unfair, then deal 1 card each until deck is empty
    while my_deck.remaining>=(call_deal_ntimes_in_turn*2):
        for call in xrange(call_deal_ntimes_in_turn):
            p1.hand.get_cards(my_deck, 1)
        for call in xrange(call_deal_ntimes_in_turn):
            p2.hand.get_cards(my_deck, 1)
    while my_deck.remaining>0:
        p1.hand.get_cards(my_deck, 1)
        p2.hand.get_cards(my_deck, 1)

    # show the hands
    p1.hand.print_hand()
    p2.hand.print_hand()

    # start scoring the hands (by number of cards in hand)
    p1.score= len(p1.hand.Cards)
    p2.score= len(p2.hand.Cards)

    # keep track of which card you are 'playing'
    # keep track of round of play
    card_index=0
    round_of_play=0
    extra_shuffles=0


    # play until a player is out of cards
    while p1.score > 0 and p2.score > 0:
    
        ############################################################# Battle

        # re-shuffle if the game is in oscillation
        if (round_of_play%1000)==0 and round_of_play!=0:
            import random
            random.shuffle(p1.hand.Cards)
            random.shuffle(p2.hand.Cards)
            extra_shuffles+=1

        #assign warcard, flip warcard, print warcard AND print # of cards in player hands
        p1, p2, p1warcard, p2warcard = flip_assign(p1, p2, card_index)
        printscores(p1, p2)
        printwarcards (p1, p2, p1warcard, p2warcard )

        # logically enter the battle_sequence dependent on which players battle 
        # card ranks highest - if they are the same ranking, enter war
        if p1.hand.Cards[card_index].rank > p2.hand.Cards[card_index].rank:

            p1,p2 =battle_sequence(p1, p2, round_of_play, card_index)

        elif p1.hand.Cards[card_index].rank < p2.hand.Cards[card_index].rank:

            p2,p1 =battle_sequence(p2, p1, round_of_play, card_index)

        else:
            
            ############################################################# War
            print "***WAR**********************************"

            # play in war mode while warcards are of equal rank
            while p1warcard.rank == p2warcard.rank:

                card_index+=2

                # if there are more cards in the decks then where the next warcard should
                # be (2 up the hand from the last card_index)- the next warcards are 2 cards in 
                if (len(p1.hand.Cards) > card_index) and (len(p2.hand.Cards) > card_index):
                     print "***WARCARDS*****************************"

                     #assign warcard, flip warcard, print flipped warcard
                     p1, p2, p1warcard, p2warcard =flip_assign(p1, p2, card_index) 
                     printwarcards (p1, p2, p1warcard, p2warcard )

                # if there is 1 less cards in either deck then where the next warcard should
                # be (2 up the hand from the last card_index)- the next warcards are 1 cards in 
                elif (len(p1.hand.Cards) > card_index-1) and (len(p2.hand.Cards) > card_index-1):
                     print "***WARCARDS****************************"

                     #assign warcard, flip warcard, print flipped warcard
                     p1, p2, p1warcard, p2warcard =flip_assign(p1, p2, card_index-1)
                     printwarcards (p1, p2, p1warcard, p2warcard )

                # if there are no further cards to play, and the last warcard was a tie, game over.
                else:
                     print "::~~~~~~::"
                     print "::~~~~~~::"
                     print "Game over."
                     print "with extra shuffles %d" %extra_shuffles
                     if p1.score > p2.score:
                        print "P1 wins."
                     else:
                        print "P2 wins"
                     sys.exit()


                # logically enter the war_sequence dependent on which players warcard 
                # ranks highest - if they are the same ranking, re-enter war
                if p1warcard.rank > p2warcard.rank:

                    p1,p2 =war_sequence(p1, p2, round_of_play, card_index)

                elif p1warcard.rank < p2warcard.rank:

                    p2,p1 =war_sequence(p2, p1, round_of_play, card_index)

                else:
                    continue

                # cards have been re-assigned accordingly, continue at the front card in hands
                card_index=0
        round_of_play=round_of_play+1

    # when one player holds no cards, print the winner
    if p1.score>0:
        print "with extra shuffles %d" %extra_shuffles
        print p1.hand.name +" WINS THE WAR!"
    else:
        print "with extra shuffles %d" %extra_shuffles
        print p2.hand.name +" WINS THE WAR!"
    

if __name__=="__main__":
    
    sys.exit(main())
